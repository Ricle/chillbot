from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import relationship
from shema import Base




class User(Base):
    __tablename__='user'

    id = Column(Integer, primary_key=True)
    telegram_id = Column(Integer)
    name = Column(String)
    fullname = Column(String)
    user_comments = relationship('DailyComment', backref='owner')

    def __repr__(self):
        return f'id - {self.id}, telegram id - {self.telegram_id}\n'

class DailyComment(Base):

    __tablename__='daily_comment'

    comment_id = Column(Integer, primary_key=True)
    comment = Column(String)
    created_at = Column(DateTime(timezone=True))
    author = Column(Integer, ForeignKey('user.id'))

    def __repr__(self):
        return f'Text - {self.comment}\nCreated_at - {self.created_at}\nAuthor {self.author}'
