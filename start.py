import config
import dbworker
import telebot

bot = telebot.TeleBot(config.TOKEN)

@bot.message_handler(commands=["start"])
def send_cmd(message):
    keyboard = telebot.types.ReplyKeyboardMarkup(row_width=2,
                                                resize_keyboard=True,
                                                one_time_keyboard=True)

    button_1 = telebot.types.KeyboardButton(text=config.write)
    button_2 = telebot.types.KeyboardButton(text=config.breath)
    button_3 = telebot.types.KeyboardButton(text=config.meditate)
    button_4 = telebot.types.KeyboardButton(text=config.thank)
    button_5 = telebot.types.KeyboardButton(text=config.manage)

    keyboard.add(button_1, button_2)
    keyboard.add(button_3, button_4)
    keyboard.add(button_5)

    bot.send_message(message.from_user.id, config.WELCOME_TEXT, reply_markup=keyboard)

@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    if call.data == "audio":
        with open("audio.mp3", "rb") as audio:
            bot.send_message(call.message.chat.id, "Ок, сейчас подготавливаю файл")
            bot.send_audio(call.message.chat.id, audio, caption=config.CAPTION)

    elif call.data == "wrotten_thank":
        bot.send_message(call.message.chat.id, "Ок, выбери три события(или больше), которые наполняют чувством благодарности")

    elif call.data == "thank":
        bot.send_message(call.message.chat.id, config.CONTACTS)

    elif call.data == "hw":
        bot.send_message(call.message.chat.id, config.WELCOME_TEXT)


@bot.message_handler(content_types=["text"])
def any_msg(message):

    if message.text == config.write:
        bot.send_message(message.from_user.id, config.ENTER_TEXT)
        dbworker.set_state(message.from_user.id, config.States.S_ENTER_TEXT)

    elif message.text == config.breath:
        pass

    elif message.text == config.meditate:
        bot.send_message(message.from_user.id, config.MEDITATE)
        key = telebot.types.ReplyKeyboardMarkup()





    elif message.text == config.thank:
         key = telebot.types.InlineKeyboardMarkup()

         btn = telebot.types.InlineKeyboardButton(text=config.write, callback_data="wrotten_thank")
         btn2 = telebot.types.InlineKeyboardButton(text="Метта", callback_data="audio")

         key.add(btn)
         key.add(btn2)

         bot.send_message(message.from_user.id, config.MANAGE, reply_markup=key)

    elif message.text == config.manage:
        key = telebot.types.InlineKeyboardMarkup()

        btn = telebot.types.InlineKeyboardButton(text=config.GET_TIMER, callback_data="timer")
        btn_1 = telebot.types.InlineKeyboardButton(text=config.GET_DICT, callback_data="dict")
        btn_2 = telebot.types.InlineKeyboardButton(text=config.THANK, callback_data="thank")
        btn_3 = telebot.types.InlineKeyboardButton(text=config.HOW_WORKS, callback_data="hw")

        key.add(btn)
        key.add(btn_1)
        key.add(btn_2)
        key.add(btn_3)

        bot.send_message(message.from_user.id, config.MANAGE, reply_markup=key)






bot.polling(none_stop=True)
