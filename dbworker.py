from models import DailyComment, User
from sqlalchemy.orm import sessionmaker
import datetime
from shema import *


def set_state(user_id, state):
    pass

#Creation a database
Session = sessionmaker(connection)
session = Session()
Base.metadata.create_all(connection)


#Basic Pre-workflow functions
def return_id(obj):
    user = session.query(User).filter(User.telegram_id==obj['from']['id'])
    for i in user:
        return i.id

def return_from_iso(date):
    return datetime.datetime.fromisoformat(date)

def return_a_timedelta(obj):
    time_to_iso = str(obj['date'])
    delta = float(obj['text'])
    from_iso = datetime.datetime.fromisoformat(time_to_iso)
    t_delta = datetime.timedelta(days=delta)

    filter = from_iso - t_delta

    return filter

#Create an objects which to write into db
def return_user_obj(obj):
    user_id = obj['from']['id']
    user_name = obj['from']['first_name']
    full_user_name = obj['from']['username']
    user = User(
         telegram_id = user_id,
         name = user_name,
         fullname = full_user_name)
    return user

def write_into_dairy(obj, user_obj):
    message_text = obj['text']
    date_pub = datetime.datetime.utcnow()

    commentary = DailyComment(
    comment = message_text,
    owner = user_obj,
    created_at = date_pub
    )

    return commentary

#Sort
def sort_by_day(obj):
    filter_obj = return_a_timedelta(obj)
    author_id = return_id(obj)

    posts = session.query(DailyComment).filter(
                    DailyComment.author==author_id).filter(
                    DailyComment.created_at>filter_obj
                    )
    get_a_post_info(posts)
    return posts

#Working with db data
def get_a_post_info(posts): #Need a data from sort_by_day
    #Return a list with comment a nd created date
    #['text', '22-01-2012']
    total = [[i.comment, i.created_at.date().strftime('%Y-%m-%d')] for i in posts]
    return total

#Create dairy post
def create_dairy_post(obj):
    user_obj = session.query(User).get(return_id(obj))
    if user_obj is not None:
        comment = write_into_dairy(obj, user_obj)
        session.add(comment)
        session.commit()
    else:
        user = return_user_obj(obj)
        session.add(user)
        session.commit()
        comment = write_into_dairy(obj, user)
        session.add(comment)
        session.commit()


def set_state(user_id, state):
    pass
