from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

import datetime

engine = create_engine('postgresql://postgres:postgres@localhost:5432/chillbot_db')

connection = engine.connect()


Base = declarative_base()
