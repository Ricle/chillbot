from enum import Enum


TOKEN = "990299617:AAHYQJYHPUR_ztMduLfZ7LBEvAmJIMOwMsM"

CAPTION = "🌻 @lounge_cafe"

WELCOME_TEXT = "Привет 👋 Я - Monkey, \n\nЯ помогу тебе лучше узнать себя и стать счастливее"
MEDITATION = """Медитация возвращает ум в состояние покоя. 

Медитируй регулярно, чтобы ощутить эффект. Для удобства ты можешь поставить напоминание о практике. 
Напиши мне команду /mystrike, чтобы увидеть сколько дней подряд ты уже медитируешь. 

Прослушай базовую медитацию из приложения 📱Silence."""

write = "✒️ Написать"
breath = "🍃 Дышать"
meditate = "🧘‍♂️ Медитировать"
thank = "🙏 Благодарить"
manage = "🎓 Управлять"

ENTER_TEXT = "Опишите коротко, \nчто произошло? Опирайтесь на факты события."
MANAGE = "Выбери нужную тебе функцию 😀:"



GET_TIMER = "Поставить ежедневное напоминание"
GET_DICT = "Загрузить дневник"
THANK = "Сказать спасибо"
HOW_WORKS = "Как всё устроенно"

CONTACTS = "Пишите, буду рад послушать: \nTelegram - @Logger2"

class States(Enum):
    S_START = "0" #Новый диалог
    S_ENTER_TEXT = "1"
    S_ENTER_THINKS = "3"
    S_ENTER_NUMS = "4"
    S_ENTER_NEW_THINK = "5"
